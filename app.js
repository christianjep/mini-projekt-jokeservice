var express = require('express');
var hbs = require('express-handlebars');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var path = require('path');
var request = require('request');

var app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(express.static('public'));
app.engine('hbs', hbs({extname: 'hbs'}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.set('port', (process.env.PORT || 8080));

// We pick the default Promise implementation
mongoose.Promise = global.Promise;

var url = "mongodb://admin:8Rw21pxA@ds117931.mlab.com:17931/jokeservice";
mongoose.connect(url, function (err, res) {
    if (err) {
        console.log('ERROR connecting to mLab database: ' + url + '. ' + err);
    } else {
        console.log('Succeesfully connected to mLab database: ' + url);
    }
});

var Joke = require('./models/Joke');
var router = express.Router();

// Funktion til at registrere egen webservice på JokeRegistry. Køres når app'en startes
function registerService() {
    var service = ({
        "name": "Funny Joke Service",
        "address": "http://funnyjokeservice.herokuapp.com/",
        "secret": "funny"
    });
    request.post('https://krdo-joke-registry.herokuapp.com/api/services/',
        {json: true, body: service},
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(body);
            } else {
                console.log("Couldn't register service in JokeRegistry!");
            }
        }
    )
};
registerService();

// Funktion til at hente Joke Service URL ud fra ID
function getJokeServiceURLById(id, callback) {
    request('https://krdo-joke-registry.herokuapp.com/api/services/', function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var services = JSON.parse(body);
            for (var service of services) {
                if (service._id == id) {
                    if (service.address.charAt(service.address.length - 1) == "/") {
                        callback(service.address);
                    } else {
                        callback(service.address + "/");
                    }
                }
            }
        }
    });
};

// Template til index.hbs vises når brugeren besøger applikationen på /
router.route('/')
    .get(function (req, res) {
        Joke.find(function (err, jokes) {
            if (err) {
                res.send(err);
            } else {
                res.render('index', {jokes: jokes})
            }
        });
    });

// Hent eksterne Joke Services
router.route('/api/services')
    .get(function (req, res) {
        request('https://krdo-joke-registry.herokuapp.com/api/services/', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                res.send(body);
            } else {
                console.log("Couldn't GET services from JokeRegistry");
            }
        });
    });

// Hent jokes fra ekstern Joke Service
router.route('/api/services/:id')
    .get(function (req, res) {
        getJokeServiceURLById(req.params.id, function (url) {
            request(url + 'api/jokes/', function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    var jokes = JSON.parse(body);
                    if (jokes.length > 0) {
                        res.send(jokes);
                    } else {
                        var jokes = [{"setup": "No jokes added to this Joke Service yet!", "punchline": ""}];
                        res.send(jokes);
                    }
                } else {
                    var error = [{"setup": "API GET FAILED: Couldn't get api/jokes/ JSON", "punchline": ""}];
                    res.send(error);
                }
            });
        });
    });

// Hent jokes fra egen Joke Service
router.route('/api/jokes')
    .get(function (req, res) {
        Joke.find(function (err, jokes) {
            if (err) {
                res.send(err);
            } else {
                res.json(jokes);
            }
        });
    })
    // Gem jokes til i egen Joke Service
    .post(function (req, res) {
        var joke = new Joke({
            setup: req.body.setup,
            punchline: req.body.punchline
        });
        joke.save(function (err) {
            if (err) {
                res.status(500);
                res.send(err);
            } else {
                res.json(joke);
            }
        });
    });

// Hent en specifik joke med ID fra egen Joke Service
router.route('/api/jokes/:id')
    .get(function (req, res) {
        Joke.findOne({'_id': req.params.id}, function (err, joke) {
            if (err) {
                res.send(err);
            } else {
                res.json(joke);
            }
        });
    });

app.use('/', router);

app.listen(app.get('port'), function () {
    console.log('Node app is running on port', app.get('port'));
});