var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var joke = new Schema({
    setup: String,
    punchline: String
});
joke.methods.toString = function() {
    return this.setup + " " + this.punchline;
};

module.exports = mongoose.model('Joke', joke);