// click handler til add joke
$('.add-joke-form > input[type=submit]').on('click', function (event) {
    event.preventDefault();

    var setup = $('#joke-setup');
    var punchline = $('#joke-punchline');

    if (setup.val() === '' || punchline.val() === '') {
        alert('Please enter setup and punchline!');
    } else {
        $.post('api/jokes', {
            setup: setup.val(),
            punchline: punchline.val()
        })
        $('.our-jokes').append('<p><b>Setup:</b> ' + setup.val() + '</p>');
        $('.our-jokes').append('<p><b>Punchline:</b> ' + punchline.val() + '</p>');
        setup.val('');
        punchline.val('');
    }
});

// append joke server titles til listen
$.get("/api/services")
    .done(function (data) {
        for (var d of JSON.parse(data)) {
            $('.joke-wrapper').append($('<div class="jokeService" data-id="' + d._id + '">'
                + '<h3>' + d.name + '</h3>' +
                '</div>'));
        }
    });

// click handler til jokes fra extern service
$('.joke-wrapper').on('click', ' > .jokeService', function (event) {
    var id = $(event.target).closest('div').data('id');
    var jokes = $.get('/api/services/' + id);
    var jokeService = $(event.target).closest('div');

    if ($(event.target).hasClass('exp')) {
        $(event.target).addClass('sub');
        $(event.target).removeClass('exp');

        $(jokeService).find('p').remove();
        $(jokeService).find('br').remove();
    } else {
        jokes.done(function (jokes) {
            for (var joke of jokes) {
                $(event.target).addClass('exp');
                $(event.target).removeClass('sub');
                jokeService.append('<p><b>Setup:</b> ' + joke.setup + '</p>');
                jokeService.append('<p><b>Punchline:</b> ' + joke.punchline + '</p>');
            }
        });
    }
});